import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class SettingsService {

	constructor(
		private translateService: TranslateService
	) { }

	initAppLang = (): void => {
		this.setLanguages();
		this.setLanguage();
	};

	setLanguages = (): void => this.translateService.addLangs(['ca', 'es', 'en']);

	setLanguage = (): void => {
		this.translateService.setDefaultLang('en');
		this.translateService.use(this.translateService.getBrowserLang());
	};

	changeLanguage = (lang: string): Observable<void> => this.translateService.use(lang);

}
