import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { UsersComponent } from './users.component';
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserSearchComponent } from './user-search/user-search.component';

// Routes
const USERS_ROUTES: Routes = [
	{
		path: '',
		component: UsersComponent,
		// canActivate: [AuthGuardService],
		children: [
			{
				path: '', redirectTo: 'user', pathMatch: 'full'
			},
			{
				path: 'user',
				component: UserComponent,
				// canActivate: [AuthGuardService]
			},
			{
				path: 'user-detail',
				component: UserDetailComponent,
				// canActivate: [AuthGuardService]
			},
			{
				path: 'user-search',
				component: UserSearchComponent,
				// canActivate: [AuthGuardService]
			}
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(USERS_ROUTES)
	],
	exports: [RouterModule]
})
export class UsersRoutingModule { }
