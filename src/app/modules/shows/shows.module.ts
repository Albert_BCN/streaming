import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

// Primeng
import { PanelModule } from 'primeng/panel';
import { CardModule } from 'primeng/card';
import { DividerModule } from 'primeng/divider';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ListboxModule } from 'primeng/listbox';

// Module
import { ShowsRoutingModule } from './shows-routing.modules';
import { ShowSearchFilterComponent } from './shows-search/show-search-filter/show-search-filter.component';
import { ShowSearchResultComponent } from './shows-search/show-search-result/show-search-result.component';
import { ShowsSearchComponent } from './shows-search/shows-search.component';
import { ShowsComponent } from './shows.component';

@NgModule({
	declarations: [
		ShowsComponent,
		ShowsSearchComponent,
		ShowSearchFilterComponent,
		ShowSearchResultComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		ShowsRoutingModule,
		TranslateModule,

		// Primeng
		PanelModule,
		CardModule,
		DividerModule,
		SelectButtonModule,
		ListboxModule
	],
	providers: []
})
export class ShowsModule { }
