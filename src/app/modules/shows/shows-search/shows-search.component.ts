import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IShowsFilter } from './shows-search.model';

@Component({
	selector: 'app-shows-search',
	templateUrl: './shows-search.component.html',
	styleUrls: ['./shows-search.component.sass']
})
export class ShowsSearchComponent implements OnInit, OnDestroy {

	// Form
	filters: FormGroup;
	filterGenre: IShowsFilter[] = [];
	filterDecade: IShowsFilter[] = [];

	// Results
	results: any[] = [];

	private unsubscribe: Subject<void> = new Subject<void>();

	constructor(
		private fb: FormBuilder,
		private translateService: TranslateService
	) { }

	ngOnInit(): void {
		this.initForm();
		this.getGenreList();
		this.getDecadeList();
	}

	ngOnDestroy(): void {
		this.unsubscribe.next();
		this.unsubscribe.complete();
	}

	// Form > Initialize
	initForm = (): void => {
		this.filters = this.fb.group({
			genre: [],
			decade: []
		});
	};

	// Form > Field: genre > Get list
	getGenreList = (): void => {
		this.translateService.get('SHOWS.SEARCH.FILTERS.GENRE').pipe(takeUntil(this.unsubscribe))
			.subscribe((translation: { [x: string]: string }) => {
				this.filterGenre = [
					{ id: '1', label: translation['1'] },
					{ id: '2', label: translation['2'] },
					{ id: '3', label: translation['3'] },
					{ id: '4', label: translation['4'] }
				];
			}
			);
	};

	// Form > Field: decade > Get list
	getDecadeList = (): void => {
		this.translateService.get('SHOWS.SEARCH.FILTERS.DECADE').pipe(takeUntil(this.unsubscribe))
			.subscribe((translation: { [x: string]: string }) => {
				this.filterDecade = [
					{ id: '1', label: translation['1'] },
					{ id: '2', label: translation['2'] },
					{ id: '3', label: translation['3'] },
					{ id: '4', label: translation['4'] },
					{ id: '5', label: translation['5'] },
					{ id: '6', label: translation['6'] },
					{ id: '7', label: translation['7'] }
				];
			}
			);
	};

}
