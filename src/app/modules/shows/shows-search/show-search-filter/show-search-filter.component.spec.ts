import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSearchFilterComponent } from './show-search-filter.component';

describe('ShowSearchFilterComponent', () => {
  let component: ShowSearchFilterComponent;
  let fixture: ComponentFixture<ShowSearchFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowSearchFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
