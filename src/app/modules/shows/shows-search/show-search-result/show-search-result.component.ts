import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-show-search-result',
	templateUrl: './show-search-result.component.html',
	styleUrls: ['./show-search-result.component.sass']
})
export class ShowSearchResultComponent implements OnInit {

	constructor() { }

	ngOnInit(): void {
	}

}
