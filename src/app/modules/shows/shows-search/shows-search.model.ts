export interface IShowsFilter {
	id: string;
	label: string;
}
