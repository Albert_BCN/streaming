import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { ShowsComponent } from './shows.component';
import { ShowsSearchComponent } from './shows-search/shows-search.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { SeriesDetailComponent } from './series-detail/series-detail.component';

// Routes
const SHOWS_ROUTES: Routes = [
	{
		path: '',
		component: ShowsComponent,
		// canActivate: [AuthGuardService],
		children: [
			{
				path: '', redirectTo: 'shows-search/movies', pathMatch: 'full'
			},
			{
				path: 'shows-search/:show-type',
				component: ShowsSearchComponent,
				// canActivate: [AuthGuardService]
			},
			{
				path: 'movie-detail',
				component: MovieDetailComponent,
				// canActivate: [AuthGuardService]
			},
			{
				path: 'series-detail',
				component: SeriesDetailComponent,
				// canActivate: [AuthGuardService]
			}
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(SHOWS_ROUTES)
	],
	exports: [RouterModule]
})
export class ShowsRoutingModule { }
