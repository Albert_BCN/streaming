import { Injectable } from '@angular/core';
import { IMenuItem } from './top-bar.model';

@Injectable({
	providedIn: 'root'
})
export class TopBarService {

	setMenu = (): IMenuItem[] => [
		{
			id: 'search-movies',
			path: '/shows/shows-search/movies',
			icon: null,
			disabled: false,
			label: 'TOP_BAR.MENU_OPTIONS.movies'
		},
		{
			id: 'search-series',
			path: '/shows/shows-search/series',
			icon: null,
			disabled: false,
			label: 'TOP_BAR.MENU_OPTIONS.series'
		},
		{
			id: 'users',
			path: '/users',
			icon: null,
			disabled: false,
			label: 'TOP_BAR.MENU_OPTIONS.users'
		}
	];

}
