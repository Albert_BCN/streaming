export interface IMenuItem {
	id: string;
	path: string;
	icon?: string;
	label?: string;
	disabled?: boolean;
}
