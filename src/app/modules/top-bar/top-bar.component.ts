import { Component, OnInit } from '@angular/core';
import { IMenuItem } from './top-bar.model';
import { TopBarService } from './top-bar.service';

@Component({
	selector: 'app-top-bar',
	templateUrl: './top-bar.component.html',
	styleUrls: ['./top-bar.component.sass']
})
export class TopBarComponent implements OnInit {

	menuOptions: IMenuItem[] = [];

	constructor(
		public topBarService: TopBarService
	) { }

	ngOnInit(): void {
		this.setMenu();
	}

	setMenu = (): void => { this.menuOptions = this.topBarService.setMenu(); };

}
