import { Component } from '@angular/core';
import { SettingsService } from './core/services/settings.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent {

	constructor(
		private settingsService: SettingsService
	) {
		// App language/translations
		this.settingsService.initAppLang();
	}

}
