import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: '', redirectTo: 'shows', pathMatch: 'full'
	},
	{
		path: 'shows',
		loadChildren: () => import('./modules/shows/shows.module').then((m) => m.ShowsModule),
		// canActivate: [AuthGuardService]
	},
	{
		path: 'users',
		loadChildren: () => import('./modules/users/users.module').then((m) => m.UsersModule),
		// canActivate: [AuthGuardService]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
